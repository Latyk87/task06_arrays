package com.epam.view;

import com.epam.Application;
import com.epam.controller.Controller;
import com.epam.controller.ControllerIml;
import com.epam.model.arrays.ArrayCopy;
import com.epam.model.arrays.ArraySearch;
import com.epam.model.collections.ComparingLogic;
import com.epam.model.collections.ConteinerString;
import com.epam.model.collections.DequeTemplate;
import com.epam.model.generics.PriorityGenerics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;



/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    DequeTemplate dequeTemplate;
    PriorityGenerics linkqueue;
    ArrayCopy linkarrays;
    ArraySearch arraysearch;
    ConteinerString conteinerstring;
    ComparingLogic comparinglogic;

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));

    public View() {
        controller = new ControllerIml();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Container with units");
        menu.put("2", "  2 - PriorityQueue");
        menu.put("3", "  3 - Array сopy and sorting ");
        menu.put("4", "  4 - Deleting digits that repeat more then two times");
        menu.put("5", "  5 - Performance ArrayList vs Array ");
        menu.put("6", "  6 - Comparable in ArrayList and Array");
        menu.put("7", "  7 - Deque class");
        menu.put("Q", "  Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("Q", this::pressButton8);
    }

    private void pressButton1() {
        logger1.info(controller.getDinosaurs());
    }

    private void pressButton2() {
        linkqueue=new PriorityGenerics();
        linkqueue.buildQueue();
    }

    private void pressButton3() {
       linkarrays=new ArrayCopy();
       linkarrays.sendArrays();
    }

    private void pressButton4() {
        arraysearch = new ArraySearch();
        arraysearch.takeArray();
    }

    private void pressButton5() {
        conteinerstring = new ConteinerString();
    }
    private void pressButton6() {
        comparinglogic = new ComparingLogic();
    }
    private void pressButton7() {
        dequeTemplate=new DequeTemplate();
        dequeTemplate.dequeExample();
    }
    private void pressButton8() {
        logger1.info("Bye-Bye");
    }
    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nEXCEPTIONS:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


