package com.epam.controller;

import com.epam.model.Model;
import com.epam.model.generics.Container;
import com.epam.model.generics.Dinosaur;
import com.epam.model.generics.Generics;
import com.epam.model.generics.PriorityGenerics;

import java.util.List;


/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

     Model link;
public ControllerIml(){
    link= new Generics();

    }

    @Override
 public  List<Container> getDinosaurs() {

        return link.getDinosaurs();
    }
}
