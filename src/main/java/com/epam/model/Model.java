package com.epam.model;

import com.epam.model.generics.Container;

import java.util.List;

/**
 * Interface for Model.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public interface Model {
    List<Container> getDinosaurs();

}
