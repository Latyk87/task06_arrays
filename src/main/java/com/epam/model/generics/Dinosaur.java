package com.epam.model.generics;

/**
 * Main class for all  Dinosaurs.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public abstract class Dinosaur {

    private String name;

    public Dinosaur(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
