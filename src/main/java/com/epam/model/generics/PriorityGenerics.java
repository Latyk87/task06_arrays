package com.epam.model.generics;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.PriorityQueue;
/**
 * PriorityQueue class using generics.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class PriorityGenerics {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public void buildQueue() {
        PriorityQueue<String> queue2
                = new PriorityQueue<>(Collections.reverseOrder());
        queue2.offer("Донецьк");
        queue2.offer("Київ");
        queue2.offer("Харків");
        queue2.offer("Львів");
        queue2.offer("Полтава");
        queue2.offer("Луцьк");
        queue2.offer("Вінниця");
        while (queue2.size() > 0) {
            logger1.info(queue2.remove() + " ");
        }
    }
}