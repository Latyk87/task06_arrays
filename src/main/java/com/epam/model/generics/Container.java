package com.epam.model.generics;

/**
 * Class is a container for all Dinosaurs.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class Container<T extends Dinosaur> {

    private T contents;

    public void setContents(T contents) {
        this.contents = contents;
    }

    public T getContents() {
        return contents;
    }

    @Override
    public String toString() {
        return contents.getClass() + " " + contents.getName();

    }
}

