package com.epam.model.generics;

import com.epam.model.Model;

import java.util.LinkedList;
import java.util.List;
/**
 * Class add the Dinosaurs to the Container.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class Generics implements Model {
    List<Container> containers = new LinkedList<>();

    public Generics() {

        Container<Triceratops> triceratopsContainer = new Container<>();
        Container<Tiranosaur> tiranosaurContainer = new Container<>();
        Container<Raptor> raptorContainer = new Container<>();

        triceratopsContainer.setContents(new Triceratops("Spark"));
        tiranosaurContainer.setContents(new Tiranosaur("T-Rex"));
        raptorContainer.setContents(new Raptor("Seg"));


        containers.add(tiranosaurContainer);
        containers.add(triceratopsContainer);
        containers.add(raptorContainer);
    }
    @Override
    public List<Container> getDinosaurs() {

        return containers;
    }
}


