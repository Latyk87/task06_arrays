package com.epam.model.generics;

/**
 * Class describes Tiranosaur Dinosaurs.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class Tiranosaur extends Dinosaur {
    public Tiranosaur(String name) {
        super(name);
    }
}
