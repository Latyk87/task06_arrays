package com.epam.model.generics;
/**
 * Class describes Triceratops Dinosaurs.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class Triceratops extends Dinosaur {
    public Triceratops(String name) {
        super(name);
    }
}
