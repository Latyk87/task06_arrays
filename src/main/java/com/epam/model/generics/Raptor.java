package com.epam.model.generics;

/**
 * Class describes Raptor Dinosaurs.
 * Created by Borys Latyk on 21/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class Raptor extends Dinosaur {
    public Raptor(String name) {
        super(name);
    }
}
