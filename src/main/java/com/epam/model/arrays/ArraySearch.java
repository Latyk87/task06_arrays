package com.epam.model.arrays;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Class is erasing digits in Array that repeat more then two times.
 * Created by Borys Latyk on 24/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class ArraySearch {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    static int[] array1 = {1, 2, 5, 6, 8, 9, 5, 3, 2, 5, 6, 6, 6, 6, 2, 2, 4, 4, 4, 3, 3, 8, 8, 1, 9, 2, 0, 0, 0, 0};

    static int numbers;

    static ArrayList<Integer> listtemp = new ArrayList<>();
    static ArrayList<Integer> listunique = new ArrayList<>();
    static ArrayList<Integer> listpairs = new ArrayList<>();

    public static void takeArray() {
        searchArray(array1);
    }

    public static void searchArray(int[] a) {

        for (int i = 0; i < a.length; i++) {
            numbers = a[i];
            listtemp.add(numbers);
        }

        for (int i = listtemp.size() - 1; i > 0; i--) {
            for (int j = 0; j <= i; j++) {
                if (listtemp.get(i) == listtemp.get(j)) {
                    if (!listunique.contains(listtemp.get(j))) {
                        listunique.add(listtemp.get(j));
                    }
                }
            }
        }
        int counter;
        for (int i = listtemp.size() - 1; i >= 0; i--) {
            counter = 0;
            for (Integer temp : listtemp) {
                if (listtemp.get(i) == temp) {
                    counter++;
                }
                if (counter == 3) {
                    counter = 0;
                    if (!listpairs.contains(listtemp.get(i))) {
                        listpairs.add(listtemp.get(i));
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < listunique.size(); i++) {
            int temp = listunique.get(i);
            listpairs.add(temp);
        }
        logger1.info(listpairs);

    }

}
