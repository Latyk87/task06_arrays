package com.epam.model.arrays;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class merge two arrays and sort the unique elemets to another array.
 * Created by Borys Latyk on 24/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */

public class ArrayCopy {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    static int[] array1 = {1, 2, 5, 6, 8, 9, 5, 3, 2, 5, 6};
    static int[] array2 = {1, 6, 7, 8, 9, 4, 3, 2, 5, 6};
    static int[] array3;


    public static void sendArrays() {
        mergeArrays(array1, array2);
    }

    public static void mergeArrays(int[] a, int[] b) {
        ArrayList<Integer> numbers = new ArrayList<>();
        ArrayList<Integer> newnumbers = new ArrayList<>();

        int tempNumbers;
        int value = a.length + b.length;
        array3 = new int[value];
        int count = 0;

        for (int i = 0; i < a.length; i++) {
            array3[i] = a[i];
            count++;
        }
        System.arraycopy(b, 0, array3, count, b.length);


        for (int i = 0; i < array3.length; i++) {
            tempNumbers = array3[i];
            numbers.add(tempNumbers);
        }


        for (int i = numbers.size() - 1; i > 0; i--) {
            for (int j = 0; j <= i; j++) {
                if (numbers.get(i) == numbers.get(j)) {
                    if (!newnumbers.contains(numbers.get(j))) {
                        newnumbers.add(numbers.get(j));
                    }
                }
            }
        }
        Integer[] array4 = newnumbers.toArray(new Integer[newnumbers.size()]);
        logger1.info(Arrays.toString(array4));
    }
}






