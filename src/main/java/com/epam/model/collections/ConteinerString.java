package com.epam.model.collections;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

/**
 * Class compares performance between array and ArrayList.
 * Created by Borys Latyk on 24/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */

public class ConteinerString {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private String[] strings = new String[10000000];
    ArrayList<String> arrayList = new ArrayList<>();

    public ConteinerString() {
        long starttime1 = System.currentTimeMillis();
        for (int i = 0; i < strings.length; i++) {
            strings[i] = "s";
            String s = strings[i];
        }

        long timespent = System.currentTimeMillis() - starttime1;
        logger1.info("Array add and get, 10000000 iterations " + timespent + " ms");


        long starttime2 = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            arrayList.add("s");
            String s = arrayList.get(i);
        }

        long timespent2 = System.currentTimeMillis() - starttime2;
        logger1.info("ArrayList add and get, 10000000 iterations " + timespent2 + " ms");
    }
}