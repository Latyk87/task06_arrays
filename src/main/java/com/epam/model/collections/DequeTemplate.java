package com.epam.model.collections;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Deque;
/**
 * Deque class using generics.
 * Created by Borys Latyk on 23/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class DequeTemplate {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    Deque<String> deque = new ArrayDeque<>();

    public  void dequeExample(){
        deque.push("Dynamo");
        deque.push("Shachtar");
        deque.push("Karpaty");
        deque.push("Olimpic");
        deque.push("Lviv");
        deque.push("Oleksandria");
        deque.push("Vorskla");
        while(!deque.isEmpty()){
            logger1.info(deque.pop()+" ");
        }

    }
}
