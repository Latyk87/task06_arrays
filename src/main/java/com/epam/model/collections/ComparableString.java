package com.epam.model.collections;

/**
 * Class implements Comparable interface for correct sorting.
 * Created by Borys Latyk on 24/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */
public class ComparableString implements Comparable<ComparableString> {
    String name;

    public ComparableString(String name) {
        this.name = name;

    }
    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(ComparableString o) {

        if (this.name.length()>o.name.length()) {
            return 1;
        }else {

            return -1;
        }
    }
}