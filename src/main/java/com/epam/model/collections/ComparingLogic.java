package com.epam.model.collections;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
/**
 * Class add sorted objects to the List.
 * Created by Borys Latyk on 24/11/2019.
 *
 * @version 2.1
 * @since 24.11.2019
 */

public class ComparingLogic {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    ArrayList<ComparableString> comparedlist = new ArrayList<>();
    ComparableString[] comparedarray = new ComparableString[10];

    ComparableString a = new ComparableString("Adidas");
    ComparableString b = new ComparableString("Nike");

    public ComparingLogic() {

        for (int i = 0; i < 10; i++) {
            comparedlist.add(b);
            comparedlist.add(a);
        }
        for (int i = 0; i < comparedarray.length; i++) {
            comparedarray[i] = b;
            if (i > 5) {
                comparedarray[i] = a;
            }
        }
        Collections.sort(comparedlist);
        Arrays.sort(comparedarray);
        logger1.info(comparedlist);

        for (ComparableString ac : comparedarray) {
            logger1.info(ac + ", ");
        }
    }

}
